defmodule CustomEs do
  def write_event do
    HTTPotion.post("http://localhost:2113/streams/user",
      body: parse_json(),
      headers: [
        "Content-Type": "application/json",
        "ES-EventType": "UserAdded",
        "ES-EventId": UUID.uuid4()
      ]
    )
  end

  def parse_json() do
    Jason.encode!(%{
      name: "Darwin",
      age: "22"
    })
  end

  def read_stream do
    response =
      HTTPotion.get("http://localhost:2113/streams/user",
        headers: [
          Accept: "application/vnd.eventstore.atom+json"
        ]
      )

    Jason.decode(response.body)
  end

  def delete_stream do
    HTTPotion.delete("http://localhost:2113/streams/user")
  end
end
