defmodule CustomEs.MixProject do
  use Mix.Project

  def project do
    [
      app: :custom_es,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpotion],
      mod: {CustomEs.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:elixir_uuid, "~> 1.2"},
      {:httpotion, "~> 3.1.0"}
    ]
  end
end
